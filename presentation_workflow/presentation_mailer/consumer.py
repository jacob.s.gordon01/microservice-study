import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    contact_info = json.loads(body)
    send_mail(
        "Your presentation has been approved",
        f'{contact_info["presenter_name"]}, we\'re happy to tell you that your presentation {contact_info["title"]} has been accepted',
        "admin@conference.go",
        [contact_info["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    contact_info = json.loads(body)
    send_mail(
        "Your presentation has been rejected. nerd.",
        f'{contact_info["presenter_name"]}, we\'re thrilled to tell you that your pathetic excuse for a presentation {contact_info["title"]} has been rejected. NERD!',
        "admin@conference.go",
        [contact_info["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to rabbitmq... NERD!")
        time.sleep(2.0)
